"""pozip"""
import os
from argparse import ArgumentParser
from dataclasses import dataclass
from zipfile import ZipFile
from pathlib import Path

__version__ = "0.0.1"

@dataclass
class Args:
    source_path: Path 
    target_path: Path 
    extract: bool
    compress: bool
    verbose: bool
    version: bool

ARGS = Args('', '', False, False, False, False)

def log(message: str) -> None:
    if ARGS.verbose:
        print(message)
    return None

def unpack_from_archive(source_path: Path, target_path: Path) -> None:
    with ZipFile(source_path) as zObj:
        zObj.extractall(path=target_path)
    return None

def pack_to_archive(source_path: Path, target_path: Path) -> None:
    print("Not implemented yet...")
    return None

def validateSourcePath() -> None:
    if not ARGS.source_path.exists():
        print("Invalid source path.")
        exit(-1)
    else:
        print(f"Full source_path: {ARGS.source_path}")
        directory, filename = os.path.split(ARGS.source_path.absolute().__str__())
        log(f"Source path: {directory}")
        if ARGS.source_path.is_dir():
            log(f"Source file: {filename}")
        else:
            log(f"Source directory: {directory}")
    return None

def validateTargetPath() -> None:
    directory, filename = os.path.split(ARGS.target_path.absolute().__str__())
    if not os.path.exists(directory):
        print("Invalid target path.")
        exit(-1)
    else:
        log(f"Target path: {directory}")
        log(f"Target archive name: {filename}")
    return None

def handleArgs(args: Args) -> None:
    ARGS.__dict__ = args.__dict__.copy() # shallow copy
    try:
        if (ARGS.source_path == None) or (ARGS.target_path == None):
            raise Exception()
        ARGS.source_path = Path(ARGS.source_path)
        ARGS.target_path = Path(ARGS.target_path)
    except:
        print("Couldn't convert source_path or target_path to actual Path.")
        exit(-1)
    if ARGS.extract:
        log("Extracting archive...")
        print(ARGS.source_path)
        print(ARGS.target_path)
        validateSourcePath()
        unpack_from_archive(ARGS.source_path, ARGS.target_path)
    elif ARGS.compress:
        msg = "Compressing"
        validateSourcePath()
        if os.path.isdir():
            log(f"{msg} directory.")
        else:
            log(f"{msg} file.")
    else:
        print("Archiving failed. Define --extract or --compress")
    # paths
    return None

def main() -> None:
    global __version__
    global args
    argp = ArgumentParser(
        "POZIP",
        description="Python3 archiving utility"
    )
    argp.add_argument("source_path", action="store", help="Source path of the file/directory/archive.")
    argp.add_argument("target_path", action="store", help="Target path of the file/directory/archive.")
    argp.add_argument("-x", "--extract", action="store_true", help="Extract files from archive.")
    argp.add_argument("-c", "--compress", action="store_true", help="Compress file/directory to archive.")
    argp.add_argument("-v", "--verbose", action="store_true")
    argp.add_argument("-V", "--version", action="version", version=__version__)
    args = argp.parse_args()
    handleArgs(args)
    print("Zip operations complete.")
    return None

main()
